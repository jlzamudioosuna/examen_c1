// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package examenc6corte1;

public abstract class Empleado {
    /*ATRIBUTOS*/
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected Contrato contrato;
    /*CONSTRUCTORES*/
    // Vacío.
    public Empleado(){
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
        this.contrato = new Contrato();
    }
    // Parámetros.
    public Empleado(int nE, String nom, String dom, Contrato cont) {
        this.numEmpleado = nE;
        this.nombre = nom;
        this.domicilio = dom;
        this.contrato = cont;
    }
    // Copia.
    public Empleado(Empleado emp){
        this.numEmpleado = emp.numEmpleado;
        this.nombre = emp.nombre;
        this.domicilio = emp.domicilio;
        this.contrato = emp.contrato;
    }
    /*ENCAPSULAMIENTO*/
    // Número Empleado:
    public int getNumEmpleado() {
        return numEmpleado;
    }
    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }
    // Nombre:
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    // Domicilio:
    public String getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    // Contrato:
    public Contrato getContrato() {
        return contrato;
    }
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }
    /*IMPLEMENTACIÓN*/
    // Calcular total.
    public abstract float calcularTotal(Docente mstr);
    // Calcular Impuesto.
    public abstract float calcularImpuesto(Docente mstr);
}
