// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package examenc6corte1;

public class Contrato {
    /*ATRIBUTOS*/
    private int claveContrato;
    private String puesto;
    private float impuestoISR;
    /*CONSTRUCTORES*/
    // Vacío.
    public Contrato(){
        this.claveContrato = 0;
        this.puesto = "";
        this.impuestoISR = 0;
    }
    // Parámetros.
    public Contrato(int cC, String p, float ISR){
        this.claveContrato = cC;
        this.puesto = p;
        this.impuestoISR = ISR;
    }
    // Copia.
    public Contrato(Contrato cont){
        this.claveContrato = cont.claveContrato;
        this.puesto = cont.puesto;
        this.impuestoISR = cont.impuestoISR;
    }
    /*ENCAPSULAMIENTO*/
    //Clave Contrato:
    public int getClaveContrato() {
        return claveContrato;
    }
    public void setClaveContrato(int claveContrato) {
        this.claveContrato = claveContrato;
    }
    // Puesto:
    public String getPuesto() {
        return puesto;
    }
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
    // Impuesto ISR:
    public float getImpuestoISR() {
        return impuestoISR;
    }
    public void setImpuestoISR(float impuestoISR) {
        this.impuestoISR = impuestoISR;
    }    
}
