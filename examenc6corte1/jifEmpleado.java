// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package examenc6corte1;

import javax.swing.JOptionPane;

public class jifEmpleado extends javax.swing.JInternalFrame {

    int claveContrato;
    int numEmpleado;
    String nombre;
    String direccion;
    String puesto;
    float impuesto;
    String nivel;
    float pagoHora;
    float horasTrabajadas;
    float total;
    float pagoAd;
    float pagoIm;
    float pagoT;
    
    public jifEmpleado() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMainInfo = new javax.swing.JPanel();
        lblHorasTrabajadas = new javax.swing.JLabel();
        lblPagoHora = new javax.swing.JLabel();
        lblNivel = new javax.swing.JLabel();
        lblImpuesto = new javax.swing.JLabel();
        lblPuesto = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblNumEmpleado = new javax.swing.JLabel();
        lblClaveContrato = new javax.swing.JLabel();
        txtHorasTrabajadas = new javax.swing.JTextField();
        txtPagoHora = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtNumEmpleado = new javax.swing.JTextField();
        txtClaveContrato = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        pnlCalculos = new javax.swing.JPanel();
        lblCalculos = new javax.swing.JLabel();
        lblPagoTotal = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblPagoAdicional = new javax.swing.JLabel();
        lblPagoImpuestos = new javax.swing.JLabel();
        txtPagoAdicional = new javax.swing.JTextField();
        txtPagoImpuestos = new javax.swing.JTextField();
        txtPagoTotal = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        btnMostrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        getContentPane().setLayout(null);

        pnlMainInfo.setLayout(null);

        lblHorasTrabajadas.setText("Horas Trabajadas");
        pnlMainInfo.add(lblHorasTrabajadas);
        lblHorasTrabajadas.setBounds(40, 220, 100, 20);

        lblPagoHora.setText("Pago Por Hora");
        pnlMainInfo.add(lblPagoHora);
        lblPagoHora.setBounds(40, 200, 100, 20);

        lblNivel.setText("Nivel");
        pnlMainInfo.add(lblNivel);
        lblNivel.setBounds(40, 170, 100, 20);

        lblImpuesto.setText("Impuesto");
        pnlMainInfo.add(lblImpuesto);
        lblImpuesto.setBounds(40, 140, 100, 20);

        lblPuesto.setText("Puesto");
        pnlMainInfo.add(lblPuesto);
        lblPuesto.setBounds(40, 120, 100, 20);

        lblDireccion.setText("Dirección");
        pnlMainInfo.add(lblDireccion);
        lblDireccion.setBounds(40, 90, 100, 20);

        lblNombre.setText("Nombre");
        pnlMainInfo.add(lblNombre);
        lblNombre.setBounds(40, 70, 100, 20);

        lblNumEmpleado.setText("Num. Empleado");
        pnlMainInfo.add(lblNumEmpleado);
        lblNumEmpleado.setBounds(40, 40, 100, 20);

        lblClaveContrato.setText("Clave Contrato");
        pnlMainInfo.add(lblClaveContrato);
        lblClaveContrato.setBounds(40, 20, 100, 20);

        txtHorasTrabajadas.setEnabled(false);
        pnlMainInfo.add(txtHorasTrabajadas);
        txtHorasTrabajadas.setBounds(150, 220, 120, 22);

        txtPagoHora.setEnabled(false);
        pnlMainInfo.add(txtPagoHora);
        txtPagoHora.setBounds(150, 200, 120, 22);

        txtImpuesto.setEnabled(false);
        pnlMainInfo.add(txtImpuesto);
        txtImpuesto.setBounds(150, 140, 120, 22);

        txtPuesto.setEnabled(false);
        pnlMainInfo.add(txtPuesto);
        txtPuesto.setBounds(150, 120, 120, 22);

        txtDireccion.setEnabled(false);
        pnlMainInfo.add(txtDireccion);
        txtDireccion.setBounds(150, 90, 220, 22);

        txtNombre.setEnabled(false);
        pnlMainInfo.add(txtNombre);
        txtNombre.setBounds(150, 70, 220, 22);

        txtNumEmpleado.setEnabled(false);
        pnlMainInfo.add(txtNumEmpleado);
        txtNumEmpleado.setBounds(150, 40, 120, 22);

        txtClaveContrato.setEnabled(false);
        pnlMainInfo.add(txtClaveContrato);
        txtClaveContrato.setBounds(150, 20, 120, 22);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 - Pre Escolar", "2 - Primaria", "3 - Secundaria" }));
        cmbNivel.setEnabled(false);
        pnlMainInfo.add(cmbNivel);
        cmbNivel.setBounds(150, 170, 120, 22);

        pnlCalculos.setBackground(new java.awt.Color(255, 153, 153));
        pnlCalculos.setLayout(null);

        lblCalculos.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblCalculos.setForeground(new java.awt.Color(255, 255, 255));
        lblCalculos.setText("CALCULOS");
        pnlCalculos.add(lblCalculos);
        lblCalculos.setBounds(10, 10, 130, 30);

        lblPagoTotal.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        lblPagoTotal.setText("Pago Total");
        pnlCalculos.add(lblPagoTotal);
        lblPagoTotal.setBounds(80, 140, 90, 19);

        lblTotal.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        lblTotal.setText("Total");
        pnlCalculos.add(lblTotal);
        lblTotal.setBounds(120, 50, 40, 19);

        lblPagoAdicional.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        lblPagoAdicional.setText("Pago Adicional (+)");
        pnlCalculos.add(lblPagoAdicional);
        lblPagoAdicional.setBounds(20, 80, 150, 19);

        lblPagoImpuestos.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        lblPagoImpuestos.setText("Pago Impuestos ( - )");
        pnlCalculos.add(lblPagoImpuestos);
        lblPagoImpuestos.setBounds(10, 110, 160, 19);

        txtPagoAdicional.setEnabled(false);
        pnlCalculos.add(txtPagoAdicional);
        txtPagoAdicional.setBounds(170, 80, 130, 22);

        txtPagoImpuestos.setEnabled(false);
        pnlCalculos.add(txtPagoImpuestos);
        txtPagoImpuestos.setBounds(170, 110, 130, 22);

        txtPagoTotal.setEnabled(false);
        pnlCalculos.add(txtPagoTotal);
        txtPagoTotal.setBounds(170, 140, 130, 22);

        txtTotal.setEnabled(false);
        pnlCalculos.add(txtTotal);
        txtTotal.setBounds(170, 50, 130, 22);

        pnlMainInfo.add(pnlCalculos);
        pnlCalculos.setBounds(40, 260, 330, 170);

        btnMostrar.setText("MOSTRAR");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnMostrar);
        btnMostrar.setBounds(470, 200, 110, 70);

        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnGuardar);
        btnGuardar.setBounds(470, 120, 110, 70);

        btnNuevo.setText("NUEVO");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnNuevo);
        btnNuevo.setBounds(470, 20, 110, 70);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnCerrar);
        btnCerrar.setBounds(270, 460, 90, 23);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnLimpiar);
        btnLimpiar.setBounds(50, 460, 90, 23);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        pnlMainInfo.add(btnCancelar);
        btnCancelar.setBounds(160, 460, 90, 23);

        getContentPane().add(pnlMainInfo);
        pnlMainInfo.setBounds(0, 0, 620, 510);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        txtClaveContrato.enable(true);
        txtNumEmpleado.enable(true);
        txtNombre.enable(true);
        txtDireccion.enable(true);
        txtPuesto.enable(true);
        txtImpuesto.enable(true);
        cmbNivel.enable(true);
        txtPagoHora.enable(true);
        txtHorasTrabajadas.enable(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        txtClaveContrato.enable(false);
        txtNumEmpleado.enable(false);
        txtNombre.enable(false);
        txtDireccion.enable(false);
        txtPuesto.enable(false);
        txtImpuesto.enable(false);
        cmbNivel.enable(false);
        txtPagoHora.enable(false);
        txtHorasTrabajadas.enable(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        txtClaveContrato.setText("");
        txtNumEmpleado.setText("");
        txtNombre.setText("");
        txtDireccion.setText("");
        txtPuesto.setText("");
        txtImpuesto.setText("");
        txtHorasTrabajadas.setText("");
        txtPagoHora.setText("");
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        int op ;
        op = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Empleados", JOptionPane.YES_NO_OPTION);

        if (op == JOptionPane.YES_OPTION) {

            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // Guardando valores de los campos:
        claveContrato = Integer.parseInt(txtClaveContrato.getText());
        numEmpleado = Integer.parseInt(txtNumEmpleado.getText());
        nombre = txtNombre.getText();
        direccion = txtDireccion.getText();
        puesto = txtPuesto.getText();
        impuesto = Float.parseFloat(txtImpuesto.getText());
        nivel = (String) cmbNivel.getSelectedItem();
        horasTrabajadas = Float.parseFloat(txtClaveContrato.getText());
        pagoHora = Float.parseFloat(txtPagoHora.getText());
        // Creación de objetos:
        Contrato cont = new Contrato(claveContrato, puesto, impuesto);
        Docente mstr = new Docente(numEmpleado, nombre, direccion, cont, Integer.parseInt(nivel), horasTrabajadas, pagoHora); 
        // Realizando Calculos:
        total = mstr.getHorasLaboradas()*mstr.getPagoHoras(); 
        txtTotal.setText(
                Float.toString(total)   // CAMPO: "Total"
        ); 
        pagoAd = mstr.calcularTotal(mstr)-total; 
        txtPagoAdicional.setText(
                Float.toString(pagoAd)  // CAMPO: "Pago Adicional"
        );
        pagoIm = mstr.calcularImpuesto(mstr); 
        txtPagoImpuestos.setText(
                Float.toString(pagoIm)  // CAMPO: "Pago Impuesto"
        );
        pagoT = mstr.calcularTotal(mstr);
        txtPagoTotal.setText(
                Float.toString(pagoT)   // CAMPO: "Pago Total"
        );
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // Estableciendo valores en los campos:
        txtClaveContrato.setText(Integer.toString(claveContrato));
        txtNumEmpleado.setText(Integer.toString(numEmpleado));
        txtNombre.setText(nombre);
        txtDireccion.setText(direccion);
        txtPuesto.setText(puesto);
        txtImpuesto.setText(Float.toString(impuesto));
        cmbNivel.setSelectedItem((Object)nivel);
        txtPagoHora.setText(Float.toString(pagoHora));
        txtHorasTrabajadas.setText(Float.toString(horasTrabajadas));
        // Mostrando Calculos:
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JLabel lblCalculos;
    private javax.swing.JLabel lblClaveContrato;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblHorasTrabajadas;
    private javax.swing.JLabel lblImpuesto;
    private javax.swing.JLabel lblNivel;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNumEmpleado;
    private javax.swing.JLabel lblPagoAdicional;
    private javax.swing.JLabel lblPagoHora;
    private javax.swing.JLabel lblPagoImpuestos;
    private javax.swing.JLabel lblPagoTotal;
    private javax.swing.JLabel lblPuesto;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pnlCalculos;
    private javax.swing.JPanel pnlMainInfo;
    private javax.swing.JTextField txtClaveContrato;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtHorasTrabajadas;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmpleado;
    private javax.swing.JTextField txtPagoAdicional;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoImpuestos;
    private javax.swing.JTextField txtPagoTotal;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
