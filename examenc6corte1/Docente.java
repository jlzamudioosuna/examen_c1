// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package examenc6corte1;

public class Docente extends Empleado {
    /*ATRIBUTOS*/
    private int nivel;
    private float horasLaboradas;
    private float pagoHoras;
    /*CONSTRUCTORES*/
    // Vacío.
    public Docente(){
        super();
        this.nivel = 0;
        this.horasLaboradas = 0;
        this.pagoHoras = 0;
    }
    // Parámetros.
    public Docente(int nE, String nom, String dom, Contrato cont, int niv, float hL, float pH){
        super(nE, nom, dom, cont);
        this.nivel = niv;
        this.horasLaboradas = hL;
        this.pagoHoras = pH;
    }
    // Copia.
    public Docente(Docente mstr){
        super(mstr);
        this.nivel = mstr.nivel;
        this.horasLaboradas = mstr.horasLaboradas;
        this.pagoHoras = mstr.pagoHoras;
    }
    /*ENCAPSULAMIENTO*/
    // Nivel:
    public int getNivel() {
        return nivel;
    }
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    // Horas Laboradas:
    public float getHorasLaboradas() {
        return horasLaboradas;
    }
    public void setHorasLaboradas(float horasLaboradas) {
        this.horasLaboradas = horasLaboradas;
    }
    // Pago Horas:
    public float getPagoHoras() {
        return pagoHoras;
    }
    public void setPagoHoras(float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }
    /*ENCAPSULAMIENTO-Override*/
    // NumEmpleado:
    @Override
    public int getNumEmpleado() {
        return numEmpleado;
    }
    @Override
    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }
    // Nombre:
    @Override
    public String getNombre() {
        return nombre;
    }
    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    // Domicilio:
    @Override
    public String getDomicilio() {
        return domicilio;
    }
    @Override
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    // Contrato:
    @Override
    public Contrato getContrato() {
        return contrato;
    }
    @Override
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }
    /*IMPLEMENTACIÓN*/
    // Calcular Total.
    @Override
    public float calcularTotal(Docente mstr) {
        int nvl = mstr.getNivel();
        float total = mstr.getPagoHoras()*mstr.getHorasLaboradas();
        switch(nvl){
            case 1 -> total = total + (total * 0.35f);
            case 2 -> total = total + (total * 0.40f);
            case 3 -> total = total + (total * 0.50f);
            default -> {}
        }
        return total;    
    }
    // Calcular impuesto.
    @Override
    public float calcularImpuesto(Docente mstr){
        float total = calcularTotal(mstr);
        float impuesto = total * mstr.getContrato().getImpuestoISR();
        return impuesto;
    }
}
